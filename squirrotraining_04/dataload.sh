#!/bin/bash

CLUSTER="http://localhost"
TOKEN="46e2fc4359ba774f8bbcb6915b69dec1819a469d8e657e82b94928d3db91d7a2dffb89a6dcc72fdaa781a778ce9f5aa1f635b8fd0f1b5fccc04290b2245cfeff"
PROJECT_ID="caRd-pX2Qd61q-nbiabQcw"

squirro_data_load  -v \
    --cluster $CLUSTER \
    --project-id $PROJECT_ID \
    --token $TOKEN \